import React from "react";
import "./Header.scss";

class Header extends React.Component {
  render() {
    return (
      <header className="header">
        <div className="header_wrapper">
          <img
            className="icon"
            src="https://thumb.tildacdn.com/tild3964-3830-4539-b236-656463306161/-/resize/144x/-/format/webp/1591174252033.png"
          ></img>
          <div>
            <a>
              <img
                className="cart"
                src="https://cdn.icon-icons.com/icons2/1993/PNG/512/bag_buy_cart_market_shop_shopping_tote_icon_123191.png"
              ></img>
            </a>
            <span>0</span>

            <a>
              <img
                className="favorite"
                src="https://img.icons8.com/?size=1x&id=z3ZIkrQcnQdK&format=png"
              ></img>
            </a>
            <span>0</span>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
