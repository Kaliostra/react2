import React from "react";
import { PropTypes } from "prop-types";
import "./Button.scss"

class Button extends React.Component{
	render(){
		const {color, children, onClick} = this.props;
		return(
			<a className={"button"} onClick={onClick} style={{backgroundColor: color}}>{children}</a>
		)
	}
}

Button.propTypes = {
	onClick: PropTypes.func.isRequired,
	children: PropTypes.string.isRequired,
	color: PropTypes.string,
}
export default Button;