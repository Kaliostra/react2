import React from "react";
import Header from "./components/Header/Header";
import ProductList from "./components/ProductList/ProductList";

function App() {
  return (
    <div className="main-wrapper">
      <Header />
      <div className="inner">
        <ProductList />
      </div>
    </div>
  );
}

export default App;
